package com.java.jse.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.java.collections.Employee;

public class EmployeeTest {

	public static void main(String[] args) {
		List<Employee> employees;
		 employees = new LinkedList<>();
		 	employees.add(new Employee(101,"Madhu","Trainer",34543.43));
			employees.add(new Employee(104,"Vinay","Developer",65765.43));
			employees.add(new Employee(103,"prithvi","Engineer",77867.43));
			employees.add(new Employee(102,"Shivaji","Programmer",67683.43));
			employees.add(new Employee(102,"Shivaji","Programmer",67683.43));
			
			/*
			 * List<Employee> employeesBySalaryMoreThan50k =
			 * employeesBySalaryMoreThan50k(employees); for(Employee employee :
			 * employeesBySalaryMoreThan50k) { System.out.println(employee); }
			 */
			List<Employee> employeesBySalaryMoreThan50k = employees.stream()
			         .filter(employee -> employee.getSalary() >= 50000)
			         .collect(Collectors.toList());
			
			System.out.println(employeesBySalaryMoreThan50k);
			groupEmployeesByAge(employees);

	}

	private static Map<Integer,List<Employee>> groupEmployeesByAge(List<Employee> employees) {
		Map<Integer,List<Employee>> employeeMapByAge = new HashMap<>();
		
		return employeeMapByAge;
	}

	/*private static List<Employee> employeesBySalaryMoreThan50k(List<Employee> employees) {
		List<Employee> employeeListSalaryMoreThan50k = new ArrayList<>();
		for(Employee employee : employees) {
			if(employee.getSalary() >= 50000) {
				employeeListSalaryMoreThan50k.add(employee);
			}
		}
		return employeeListSalaryMoreThan50k;
		
	}*/

}
