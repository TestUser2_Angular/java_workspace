package com.java.oop;

public class CallByValue {
	
	 int add(int no1, int no2) {
		no1 = 10;
		no2 = 20;
		return no1 + no2;
	}

	public static void main(String[] args) {
		//main is calling method
		//add is called method
		CallByValue cbv = new CallByValue();
		System.out.println(cbv.add(100,200));
		int a = 1000, b = 2000;
		System.out.println(cbv.add(a,b));// 30
		System.out.println(a);//1000
		System.out.println(b);//2000
		a = 10000;
		b = 20000;
		System.out.println(cbv.add(a,b)); //30

	}

}
