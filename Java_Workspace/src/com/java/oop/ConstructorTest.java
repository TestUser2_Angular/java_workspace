package com.java.oop;

public class ConstructorTest {

	public static void main(String[] args) {
		Employee emp1;
		emp1 = new Employee("madhu", "training consultant", 'm', 25000.0, 2500, 2000, 1500, 2019, false);
		emp1.name = "abcd";
		System.out.println("name:" + emp1.name);
		System.out.println(emp1.designation);
		System.out.println(emp1.gender);
		System.out.println(emp1.basicSalary);
		System.out.println(emp1.hra);
		System.out.println(emp1.ta);
		System.out.println(emp1.da);
		System.out.println(emp1.joiningYear);
		System.out.println(emp1.isRegular);
		Employee emp2;
		emp2 = new Employee("vinay", "ceo", 'm', 250000.0, 25000, 20000, 15000, 2010, false);
		System.out.println(emp2.name);
		System.out.println(emp2.designation);
		System.out.println(emp2.gender);
		System.out.println(emp2.basicSalary);
		System.out.println(emp2.hra);
		System.out.println(emp2.ta);
		System.out.println(emp2.da);
		System.out.println(emp2.joiningYear);
		System.out.println(emp2.isRegular);
		
		Employee emp3 ;
		emp3 = new Employee();
		

	}
}
