package com.java.oop.p1;

 public class A {
	
	public String customerName = "customer1";
	long customerBackAccountNumber = 34534534534L;
	private int atmPin = 1234;
	String customerGender = "M";
	String bankName = "ICICI";
	protected String mobileNo = "345345345";
	

	
	//Within the same class 
	A a;
	//Within other class of same package (p1)
	
	//Within sub class of same package (p1)
	
	//Within other class of other packages (p2)
	
	//within sub class of other packages
	
	void m1() {
		A a = new A();
		System.out.println(a.customerName);
		System.out.println(a.atmPin);
		System.out.println(a.customerBackAccountNumber);
		System.out.println(a.mobileNo);
	}
	
	
	

}
