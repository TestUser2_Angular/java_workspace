package com.java.oop;

public class Product {
	//instance variable
	String name; //heap
	int price; //heap
	//static variable
	static double discount; //heap(static pool)
	//static constant
	static final int DISCOUNT_PERCENTAGE = 10;//heap (static pool)
	Product product ; //instance
	
	//instance method (stack)
	void productInfo(int productCode) { // method variables ( local variable )stack
		String productCategory =  "laptop"; //method variable (local variable) stack
		System.out.println("Name : " + this.name); //null
		System.out.println("Product Category : " + productCategory);
		System.out.println("Discount :" + discount);
		for(int i = 0 ; i < 5 ; i++) { //block variable (stack)
			System.out.println("Product " + i);	
			if(i == 2) {
				boolean repeat = false; //block variable (stack)
			}
			//System.out.println(repeat); can only be accessed inside if
		}
		//System.out.println(i); can be accessed only inside for
		System.out.println(productCategory);
		Product product = new Product();//local variables (stack)
		System.out.println(productCode);
	}

	public static void main(String[] args) {
		
		Product product; //local variable (stack)
		product = new Product();
		product.name = "Leveno Lavptop L310";
		product.price = 45645;
		product.product = new Product();
		System.out.println(product.name);
		System.out.println(product.product.name);
		
		product.productInfo(101);
	}

}
