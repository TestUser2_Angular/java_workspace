package com.java.oop;

public class StaticTest {

	//static initializer
	static {
		System.out.println("Static Block 1 is called");
	}
	
	static {
		System.out.println("Static block 2 is called");
	}
	
	public static void main(String[] args) {
		System.out.println("Main method is called");
		
		System.out.println(Employee.COMPANY_CEO);
		System.out.println(Employee.COMPANY_NAME);
		System.out.println(Employee.COMPANY_REVENUE);
		Employee.companyProfile();
		Employee emp1;
		emp1 = new Employee("Madhu Samala", "training consultant", 'm', 25000.0, 2500, 2000, 1500, 2019, false);
		
		Employee emp2;
		emp2 = new Employee("vinay", "ceo", 'm', 250000.0, 25000, 20000, 15000, 2010, false);
	
		System.out.println(emp1.name);
		System.out.println(emp2.name);
		
		System.out.println(emp1.COMPANY_NAME);
		System.out.println(emp1.COMPANY_REVENUE);
		System.out.println(emp2.COMPANY_NAME);
		System.out.println(emp2.COMPANY_REVENUE);
		
		//emp2.COMPANY_NAME = "Google";
		System.out.println(Employee.COMPANY_NAME); //Google, microsoft
		System.out.println(emp1.COMPANY_NAME); //Google , Google
		emp1.companyProfile();
		
	}

}
