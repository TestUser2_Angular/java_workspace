package com.java.oop.polymorphism;

public class Database {
	
	/*
	 * Database database = new Database();
	 * database IS-A Database
	 * database HAS-A dbName
	 * database HAS-A port
	 * database HAS-A username
	 * database HAS-A password
	 * database HAS-A connectToDb
	 */
	
	String dbName;
	int port;
	String username;
	String password;
	
	
	void connectToDb() {
		System.out.println("This will connect to oracle database");
	}

}
