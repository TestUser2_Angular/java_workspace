package com.java.oop.polymorphism;

public class MongoDB extends Database {
	
	/*
	 * MongoDB mongoDb = new MongoDB();
	 * mongoDb IS-A MongoDB
	 * mongoDb IS-A Database
	 * 
	 * mongoDb HAS-A dbUrl
	 * mongoDb HAS-A noOfDocuments
	 * 
	 * mongoDb HAS-A dbName
	 * mongoDb HAS-A port
	 * mongoDb HAS-A username
	 * mongoDb HAS-A password
	 * mongoDb HAS-A connectToDb
	 */
	String dbUrl;
	int noOfDocuments;
	
	void connectToDb() {
		System.out.println("This will connect to MongoDB");
	}
}
