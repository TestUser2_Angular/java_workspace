package com.java.oop.polymorphism;

public class Test {

	public static void main(String[] args) {
		Repository repository = new Repository();
		Database database = new Database();
		//repository.connectToDatabase(database);
		
		/*MongoDB mongoDB = new MongoDB();
		repository.connectToDatabase(mongoDB);*/
		MySQL mySQL = new MySQL();
		repository.connectToDatabase(mySQL);

	}

}
