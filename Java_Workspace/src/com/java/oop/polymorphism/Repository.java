package com.java.oop.polymorphism;

public class Repository {
	
	void connectToDatabase(Database database) {
		database.connectToDb();
	}

}
