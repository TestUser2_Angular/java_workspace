package com.java.oop.polymorphism;

public class MySQL extends Database {
	
	/*
	 * MySQL mySQL = new MySQL();
	 * mySQL IS-A MySQL
	 * mySQL IS-A Database
	 * 
	 * mySQL HAS-A role
	 * mySQL HAS-A noOfTables
	 * 
	 * mySQL HAS-A dbName
	 * mySQL HAS-A port
	 * mySQL HAS-A username
	 * mySQL HAS-A password
	 * mySQL HAS-A connectToDb
	 */
	
	String role;
	int noOfTables;
	
	void connectToDb() {
		System.out.println("This will connect to MySQL Database");
	}

}
