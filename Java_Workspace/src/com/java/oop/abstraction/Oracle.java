package com.java.oop.abstraction;

public class Oracle extends Database {
	
	String adminConfigDetails;
	
	

	public Oracle(String dbName, int port, String username, String password, String adminConfigDetails) {
		super(dbName, port, username, password);
		this.adminConfigDetails = adminConfigDetails;
	}



	@Override
	void connectToDb() {
		System.out.println("Database is Connected to Oracle");

	}

}
