package com.java.oop.abstraction;

public  abstract class Database {
	
	/*
	 * Database database = new Database();
	 * database.connectToDb();
	 * database IS-A Database
	 * database HAS-A dbName
	 * database HAS-A port
	 * database HAS-A username
	 * database HAS-A password
	 * database HAS-A connectToDb
	 */
	
	String dbName;
	int port;
	String username;
	String password;
	
   
	
	public Database(String dbName, int port, String username, String password) {
		
		this.dbName = dbName;
		this.port = port;
		this.username = username;
		this.password = password;
	}



	/*
	void connectToDb() {
		System.out.println("This will connect to oracle database");
	} */
	
	abstract void connectToDb();
	
	//non-abstract method
	void dbInfo() {
		System.out.println("Db Name : " + this.dbName);
		System.out.println("DB Port : " + this.port);
		System.out.println("DB username : " + this.username);
		System.out.println("DB Password : " + this.password);
	}

}
