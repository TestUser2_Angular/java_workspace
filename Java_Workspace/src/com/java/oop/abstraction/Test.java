package com.java.oop.abstraction;

public class Test {

	public static void main(String[] args) {
		Repository repository = new Repository();
		//Database database = new Database();
		//repository.connectToDatabase(database);
		
		/*MongoDB mongoDB = new MongoDB();
		repository.connectToDatabase(mongoDB);*/
		Oracle oracle = new Oracle("oracleempdb",1521,"oracle","oracle","adminConfig");
		repository.connectToDatabase(oracle);
		MySQL mySQL = new MySQL("mysqlempdb",3306,"mysql","mysql","admin",15);
		repository.connectToDatabase(mySQL);
		MongoDB mongoDB = new MongoDB("mongoempdb",27017,"mongo","mongo","mongo://localhost:27017",20);
		repository.connectToDatabase(mongoDB);
		
		PostGRESQL postGreSQL = new PostGRESQL("postgreempdb", 5432, "postgre", "postgre", "postgre://localhost:5432");
		repository.connectToDatabase(postGreSQL);
		//System.out.println(mySQL.port);
		
		oracle.dbInfo();
		mySQL.dbInfo();
				
		

	}

}
