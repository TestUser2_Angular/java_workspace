package com.java.oop.abstraction;

public class PostGRESQL extends Database {
	
	String dbUrl;
	
	

	public PostGRESQL(String dbName, int port, String username, String password, String dbUrl) {
		super(dbName, port, username, password);
		this.dbUrl = dbUrl;
	}



	@Override
	void connectToDb() {
		System.out.println("Connect to PostGRESQL DB");

	}

}
