package com.java.oop.abstraction;

public class MongoDB extends Database {
	
	/*
	 * MongoDB mongoDb = new MongoDB();
	 * mongoDb IS-A MongoDB
	 * mongoDb IS-A Database
	 * 
	 * mongoDb HAS-A dbUrl
	 * mongoDb HAS-A noOfDocuments
	 * 
	 * mongoDb HAS-A dbName
	 * mongoDb HAS-A port
	 * mongoDb HAS-A username
	 * mongoDb HAS-A password
	 * mongoDb HAS-A connectToDb
	 */
	String dbUrl;
	int noOfDocuments;
	
	
	public MongoDB(String dbName, int port, String username, String password, String dbUrl, int noOfDocuments) {
		super(dbName, port, username, password);
		this.dbUrl = dbUrl;
		this.noOfDocuments = noOfDocuments;
	}


	@Override
	void connectToDb() {
		System.out.println("Database is connect to MongoDB");
		
	}
	
	
}
