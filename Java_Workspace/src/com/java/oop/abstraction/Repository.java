package com.java.oop.abstraction;

public class Repository {
	
	void connectToDatabase(Database database) {
		database.connectToDb();
	}

}
