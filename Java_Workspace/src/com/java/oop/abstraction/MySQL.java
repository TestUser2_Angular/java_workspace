package com.java.oop.abstraction;

public class MySQL extends Database {
	
	/*
	 * MySQL mySQL = new MySQL();
	 * mySQL IS-A MySQL
	 * mySQL IS-A Database
	 * 
	 * mySQL HAS-A role
	 * mySQL HAS-A noOfTables
	 * 
	 * mySQL HAS-A dbName
	 * mySQL HAS-A port
	 * mySQL HAS-A username
	 * mySQL HAS-A password
	 * mySQL HAS-A connectToDb
	 */
	
	String role;
	int noOfTables;
	
	
	
	
	public MySQL(String dbName, int port, String username, String password, String role, int noOfTables) {
		super(dbName, port, username, password);
		this.role = role;
		this.noOfTables = noOfTables;
	}




	@Override
	void connectToDb() {
		System.out.println("Database is connected to MySQL");
		
	}
	
	
}
