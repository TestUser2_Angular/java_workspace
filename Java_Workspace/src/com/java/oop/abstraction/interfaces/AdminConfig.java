package com.java.oop.abstraction.interfaces;

public interface AdminConfig {
	
	public static final String ADMIN_CODE ="admin101";
	
	void adminConfigDetails(String adminUsername, String adminPassword);

}
