package com.java.oop.abstraction.interfaces;

public class MongoDB implements IDatabase,AdminConfig {
	
	/*
	 * MongoDB mongoDb = new MongoDB();
	 * mongoDb IS-A MongoDB
	 * mongoDb IS-A Database
	 * 
	 * mongoDb HAS-A dbUrl
	 * mongoDb HAS-A noOfDocuments
	 * 
	 * mongoDb HAS-A dbName
	 * mongoDb HAS-A port
	 * mongoDb HAS-A username
	 * mongoDb HAS-A password
	 * mongoDb HAS-A connectToDb
	 */
	int portNo;
	String dbUrl;
	int noOfDocuments;
	public static final int LIMIT_OF_DOCUMENTS = 100;
	
	
	public MongoDB(int portNo, String dbUrl, int noOfDocuments) {
		
		this.portNo = portNo;
		this.dbUrl = dbUrl;
		this.noOfDocuments = noOfDocuments;
	}
	@Override
	public void connectToDb() {
		System.out.println("Logic to Connect to Mongo");
		System.out.println("Successfully connected to Mongodb");
		
	}
	@Override
	public void dbInfo() {
		
		System.out.println("DB Name : " + DB_NAME);
		System.out.println("DB Username : " + USERNAME);
		System.out.println("DB Password : " + PASSWORD);
		System.out.println("Max No of Documents : " + MongoDB.LIMIT_OF_DOCUMENTS);
		System.out.println("Port No : " + this.portNo);
		System.out.println("Database URL" + this.dbUrl);
		System.out.println("No of Documents : " + this.noOfDocuments);
		
		
	}
	@Override
	public void adminConfigDetails(String adminUsername, String adminPassword) {
		System.out.println("MongoDB implemntation of Admin Config");
		System.out.println("Admin Code : " + ADMIN_CODE );
		
	}
	
	
	
	
}
