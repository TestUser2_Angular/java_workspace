package com.java.oop.abstraction.interfaces;

public class Test {

	public static void main(String[] args) {
		Repository repository = new Repository();
		
		/*MongoDB mongoDb = new MongoDB(27017, "mongodb://localhost:27017", 15);
		
		repository.connectToDatabase(mongoDb);
		repository.databaseInfo(mongoDb); */
		
		
		IDatabase database ;
		
		//database = new MySQL("admin", 20); //Runtime Polymorphism
		database = new MongoDB(27017, "mongodb://localhost:27017", 15);
	//	database = new Oracle("adminconfig");
		
		
		MongoDB mongoDB = new MongoDB(27017, "mongodb://localhost:27017", 15);
		
		repository.connectToDatabase(database);
		repository.databaseInfo(database);
		repository.adminConfigDetails(mongoDB, "adminconfig111", "adminconfig111");
		
		
		

	}

}
