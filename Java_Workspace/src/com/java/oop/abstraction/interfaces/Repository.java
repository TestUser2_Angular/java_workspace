package com.java.oop.abstraction.interfaces;

public class Repository {
	
	void connectToDatabase(IDatabase database) {
		database.connectToDb();
	}

	
	void databaseInfo(IDatabase database) {
		database.dbInfo();
	}
	
	void adminConfigDetails(AdminConfig adminConfig, String username, String password) {
		adminConfig.adminConfigDetails(username, password);
	}
}
