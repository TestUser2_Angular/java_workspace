package com.java.oop.abstraction.interfaces;

public class MySQL implements IDatabase {
	
	
	
	String role;
	int noOfTables;
	
	
	public MySQL(String role, int noOfTables) {
		
		this.role = role;
		this.noOfTables = noOfTables;
	}
	@Override
	public void connectToDb() {
		System.out.println("Logic to Connect to MySQL");
		System.out.println("Successfully connected to MySQL");
		
		
	}
	@Override
	public void dbInfo() {
		System.out.println("DB Name : " + DB_NAME);
		System.out.println("DB Username : " + USERNAME);
		System.out.println("DB Password : " + PASSWORD);
		System.out.println("Role :" + this.role);
		System.out.println("Number of Tables" + this.noOfTables);
		
	}
	
	
	
	
	



	
}
