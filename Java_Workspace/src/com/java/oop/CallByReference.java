package com.java.oop;

public class CallByReference {
	
	void displayEmployeeProfile(Employee employee) {
		employee.displayProfile();
	}
	
	void changeValue(Random random) {
		random = new Random();
		random.name = "Good Bye";
		System.out.println(random.name); //Good Bye
	}
	

	public static void main(String[] args) {
		
		Random random = new Random();
		System.out.println(random.name);
		random.name = "Hello";
		CallByReference cbr = new CallByReference();
		/*cbr.changeValue(random);
		System.out.println(random.name); //Good Bye */
		cbr.changeValue(new Random());
		System.out.println(random.name); //Hello
		/*
		 * 
		 * 
		 */
		
		Employee emp1 = new Employee("vinay", "ceo", 'm', 250000.0, 25000, 20000, 15000, 2010, false);
		
		cbr.displayEmployeeProfile(emp1);
		

	}

}
class Random{
	String name;
}
