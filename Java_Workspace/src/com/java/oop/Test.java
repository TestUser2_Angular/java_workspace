package com.java.oop;

public class Test {

	// Write a method which takes employee object as a parameter and display
	// employee profile

	void displayEmployeeProfile(Employee employee) {
		employee.displayProfile();
	}

	// Write a method which calculates employee salary
	double calculateEmployeeSalary(Employee employee) {
		//increment employee salary by 15 percentage
		double totalSalary =  employee.totalSalaryOfEmployee();
		totalSalary += (totalSalary * 15 / 100);
		return totalSalary;
		
	}

	public static void main(String[] args) {
		// DisplayEmployeeProfile dep = new DisplayEmployeeProfile();
		
		Test test = new Test();
		Employee emp1 = new Employee("vinay", "ceo", 'm', 250000.0, 25000, 20000, 15000, 2010, false);
		test.displayEmployeeProfile(emp1);
		System.out.println("incremented Salary : " + test.calculateEmployeeSalary(emp1));
		Employee emp2 = new Employee("madhu", "training consultant", 'm', 25000.0, 2500, 2000, 1500, 2019, false);
		test.displayEmployeeProfile(emp2);

	}

}
