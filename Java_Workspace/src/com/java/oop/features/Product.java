package com.java.oop.features;

public class Product {
	
	String id;
	String name;
	double maxRetailPrice;
	float discountPercentage;
	String category;
	short manufacturingYear;
	
	public Product() {
	//	System.out.println("Product() constructor called");
	}

	public Product(String id, String name, double maxRetailPrice, String category, short manufacturingYear) {
		//System.out.println("Product(id,name...) is called");
		this.id = id;
		this.name = name;
		this.maxRetailPrice = maxRetailPrice;
		this.category = category;
		this.manufacturingYear = manufacturingYear;
	}

//Logic for calculating discount percentage
//type returntype methodname(arguments){}
//float calculateDiscountPercentage()

	float discountPercentage() {

		switch (this.category) {
		case "mobilephone": {
			if (this.manufacturingYear >= 2018) {
				this.discountPercentage = 10.5f;
			} else if (this.manufacturingYear >= 2015 && this.manufacturingYear < 2018) {
				this.discountPercentage = 20.0f;
			} else {
				this.discountPercentage = 50.0f;
			}
			break;
		}
		case "laptop": {
			if (this.manufacturingYear >= 2018) {
				this.discountPercentage = 20.0f;
			} else if (this.manufacturingYear >= 2015 && this.manufacturingYear < 2018) {
				this.discountPercentage = 25.5f;
			} else {
				this.discountPercentage = 30.5f;
			}
			break;
		}
		case "dresses": {
			if (this.manufacturingYear >= 2018) {
				this.discountPercentage = 11.0f;
			} else if (this.manufacturingYear >= 2015 && this.manufacturingYear < 2018) {
				this.discountPercentage = 22.5f;
			} else {
				this.discountPercentage = 75.5f;
			}
			break;
		}
		}

		return this.discountPercentage;

	}

	double discountPrice() {
		return (this.maxRetailPrice * this.discountPercentage() ) / 100 ;
	}
	
	double finalPrice() {
		return this.maxRetailPrice - this.discountPrice();
	}
	
	void displayProduct() {
		System.out.println("Product Id : " + this.id);
		System.out.println("Product Name : " + this.name);
		System.out.println("Product Category : " + this.category);
		System.out.println("Product MRP : " + this.maxRetailPrice);
		System.out.println("Product Manufacturing Year : " + this.manufacturingYear);
		
	}
	//method overloading in base class
	void displayProduct(String productId) {
		//Logic to display product based on id
	}
	
	void displayProduct(String category, int manufactingYear) {
		//Logic to display product based on category and manufacting year
	}

}
