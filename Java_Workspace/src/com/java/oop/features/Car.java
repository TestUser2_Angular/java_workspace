package com.java.oop.features;

public class Car extends Product {
	
	int engineCC;
	String carType;
	
	public Car(String id, String name, double maxRetailPrice, String category, short manufacturingYear, int engineCC,
			String carType) {
		super(id, name, maxRetailPrice, category, manufacturingYear);
		this.engineCC = engineCC;
		this.carType = carType;
	}
	//method overriding
	void displayProduct() {
		super.displayProduct();
		System.out.println("Car Engine CC" + this.engineCC);
		System.out.println("CAr Type  : " + this.carType);
		
	}
	
	

}
