package com.java.oop.features;

public class PolymorphismTest {
	
	//Method overloading
	void add() {}
	int add(int no) {return no;}
	int add(int no1, int no2) {return no1 + no2;}
	double add(double no1, double no2) { return no1 + no2 ;}
	double add(int no1, double no2) {return no1 + no2 ;}
	

	public static void main(String[] args) {
		Laptop laptop1 = new Laptop("p101","Lenovo Laptop L310",35500,"laptop",(short)2017,(byte)4,8192,1048576);
		Laptop laptop2 = new Laptop("p104","Dell Inspiron 305",50000,"laptop",(short)2014,(byte)8,16384,2097152);
		
		//System.out.println(laptop1.id);
		//laptop1.displayProduct();
		
		Product product = new Product("p111","Product1",11111,"product",(short)2000);
		Laptop laptop = new Laptop("p101","Lenovo Laptop L310",35500,"laptop",(short)2017,(byte)4,8192,1048576);
		Car car = new Car("c101","BMW X1",3453453,"car",(short)2021,3500,"SUV");
		
		ServiceClass sc = new ServiceClass();
		//sc.displayProduct(product);
		//sc.displayProduct(car);
		sc.displayProduct(laptop);
	}

}
