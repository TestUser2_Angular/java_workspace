package com.java.oop.features;


public class InheritanceTest {

	public static void main(String[] args) {
		/*Laptop laptop = new Laptop();
		System.out.println(laptop.id);
		Laptop laptop2 = new Laptop();
		System.out.println(laptop2.id);*/
		
		Laptop laptop1 = new Laptop("p101","Lenovo Laptop L310",35500,"laptop",(short)2017,(byte)4,8192,1048576);
		Laptop laptop2 = new Laptop("p104","Dell Inspiron 305",50000,"laptop",(short)2014,(byte)8,16384,2097152);
		
		System.out.println(laptop1.id);
		System.out.println(laptop2.id);
		System.out.println(laptop1.ramSize);
		System.out.println(laptop2.ramSize);
		
		System.out.println(laptop1.name + " 's Final Price after Discount is : " + laptop1.finalPrice());
		//laptop1.displayProduct();
		//laptop1.displayLaptop();
	}

}
