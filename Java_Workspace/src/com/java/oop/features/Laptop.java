package com.java.oop.features;

public class Laptop extends Product {
	int id;
	byte cpuSize;
	int ramSize; 
	int romSize;
	
	public Laptop() {
		super();
	}

	public Laptop(String id, String name, double maxRetailPrice, String category, short manufacturingYear, byte cpuSize,
			int ramSize, int romSize) {
		super(id, name, maxRetailPrice, category, manufacturingYear);
		this.cpuSize = cpuSize;
		this.ramSize = ramSize;
		this.romSize = romSize;
	}

	

	/*public Laptop(String id, String name, double maxRetailPrice, String category, short manufacturingYear) {
		super(id, name, maxRetailPrice, category, manufacturingYear);
		
	}*/
	
	/*void displayLaptop() {
		super.displayProduct();
		/*
		 * System.out.println(this.id); System.out.println(super.id);
		 */
	/*	System.out.println("Laptop CPU Size: " + this.cpuSize);
		System.out.println("Laptop Ram Size in Bytes : " + this.ramSize);
		System.out.println("Laptop ROM Size in Bytes : " + this.romSize);
	}*/
	//Derived class (method overriding)
	void displayProduct() {
		/*System.out.println("Product Id : " + this.id);
		System.out.println("Product Name : " + this.name);
		System.out.println("Product Category : " + this.category);
		System.out.println("Product MRP : " + this.maxRetailPrice);
		System.out.println("Product Manufacturing Year : " + this.manufacturingYear);*/
		super.displayProduct();
		System.out.println("Laptop CPU Size: " + this.cpuSize);
		System.out.println("Laptop Ram Size in Bytes : " + this.ramSize);
		System.out.println("Laptop ROM Size in Bytes : " + this.romSize);
		
	}
	
	//method overriding derived class
	void displayProduct(int cpuSize) {
		//Logic to display product based on cpuSize
	}
	
	

}
