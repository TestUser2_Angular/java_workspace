package com.java.exceptions;

import java.util.Scanner;

public class BankTransaction {

	public static void main(String[] args) {
		double balance = 20000;
		int cutOffBalance = 1000;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the amount to withdraw");
		int withdrawAmount = scanner.nextInt();

		if ((balance - cutOffBalance) >= withdrawAmount) {
			System.out.println("Transcation Success");
			balance -= withdrawAmount;
			System.out.println("Available balance : " + balance);
			printTransaction();
		} else {
			System.out.println("Low Balance");
			// Exception throw
			InsufficientFundsException ife;
			try {
				ife = new InsufficientFundsException();
				throw ife;
			} catch (InsufficientFundsException exception) {
				exception.maxAmountToWithDraw((int) (balance - cutOffBalance));
			}
		}
		
		
	}

	private static void printTransaction() {
		System.out.println("Logic to Print Transcation....");

	}

}
