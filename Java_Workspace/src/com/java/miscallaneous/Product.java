package com.java.miscallaneous;

public class Product {
	/*
	 * -- method is to calculate the discount price of a product -- The discount
	 * percentage is as follows: 1. Mobile Phones -> - Manufacturing Year >= 2018
	 * 10.5% - Manufacturing Year >= 2015 to < 2018 -> 20% - Manufacturing Year <
	 * 2015 50% 2. Laptops -> Manufacturing Year >= 2018 20% - Manufacturing Year >=
	 * 2015 to < 2018 -> 25.5% - Manufacturing Year < 2015 30.5% 3. Dresses ->
	 * Manufacturing Year >= 2018 11% - Manufacturing Year >= 2015 to < 2018 ->
	 * 22.5% - Manufacturing Year < 2015 75.5%
	 * 
	 * id,name,mrp,discountpercentage,category,manufacturingYear
	 */

	String id;
	String name;
	double maxRetailPrice;
	float discountPercentage;
	String category;
	short manufacturingYear;

	public Product(String id, String name, double maxRetailPrice, String category, short manufacturingYear) {

		this.id = id;
		this.name = name;
		this.maxRetailPrice = maxRetailPrice;
		this.category = category;
		this.manufacturingYear = manufacturingYear;
	}

//Logic for calculating discount percentage
//type returntype methodname(arguments){}
//float calculateDiscountPercentage()

	float discountPercentage() {

		switch (this.category) {
		case "mobilephone": {
			if (this.manufacturingYear >= 2018) {
				this.discountPercentage = 10.5f;
			} else if (this.manufacturingYear >= 2015 && this.manufacturingYear < 2018) {
				this.discountPercentage = 20.0f;
			} else {
				this.discountPercentage = 50.0f;
			}
			break;
		}
		case "laptop": {
			if (this.manufacturingYear >= 2018) {
				this.discountPercentage = 20.0f;
			} else if (this.manufacturingYear >= 2015 && this.manufacturingYear < 2018) {
				this.discountPercentage = 25.5f;
			} else {
				this.discountPercentage = 30.5f;
			}
			break;
		}
		case "dresses": {
			if (this.manufacturingYear >= 2018) {
				this.discountPercentage = 11.0f;
			} else if (this.manufacturingYear >= 2015 && this.manufacturingYear < 2018) {
				this.discountPercentage = 22.5f;
			} else {
				this.discountPercentage = 75.5f;
			}
			break;
		}
		}

		return this.discountPercentage;

	}

	double discountPrice() {
		return (this.maxRetailPrice * this.discountPercentage() ) / 100 ;
	}
	
	double finalPrice() {
		return this.maxRetailPrice - this.discountPrice();
	}
//calculate the discount price

//Create a class and create some product objects and display their discount amount

}
