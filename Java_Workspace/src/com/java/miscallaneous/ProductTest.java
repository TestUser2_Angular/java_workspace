package com.java.miscallaneous;

public class ProductTest {

	public static void main(String[] args) {
		Product p1 = new Product("p101","Lenovo Laptop L310",35500,"laptop",(short)2017);
		Product p2 = new Product("p102","Samsung Galaxy",45000,"mobilephone",(short)2020);
		Product p3 = new Product("p103","Human Being Men T-shirt",3500,"dresses",(short)2021);
		Product p4 = new Product("p104","Dell Inspiron 305",50000,"laptop",(short)2014);
		Product p5 = new Product("p105","Ladies Jeans",4500,"dresses",(short)2015);
		Product p6 = new Product("p106","One plus",75000,"mobilephone",(short)2017);
		Product p7 = new Product("p107","Apple Iphone 13",96000,"mobilephone",(short)2020);
		Product p8 = new Product("p108","Apple Iphone 12",85000,"mobilephone",(short)2021);
		
		//Display Each product with the discount percentage
		
		/*System.out.println(p1.name + "'s discount percentage is : " + p1.calculateDiscountPercentage() + "%");
		System.out.println(p2.name + "'s discount percentage is : " + p2.calculateDiscountPercentage() + "%");
		
		System.out.println(p3.name + "'s discount percentage is : " + p3.discountPercentage + "%");
		*/
		
		System.out.println(p1.name + "'s final price after discount is : " + p1.finalPrice() );
		System.out.println(p1.name + "'s discount price is : " + p1.discountPrice());
		
		Product[] products = {p1,p2,p3,p4,p5,p6,p7,p8};
		//Howmany laptops are there in this products list
		
		int laptopCount = countLaptops(products);
		int mobilephonesCount = countMobilePhones(products);
		int dressesCount = countDresss(products);
		System.out.println("Total Laptops : " + laptopCount);
		System.out.println("Total MobilePhones : " + mobilephonesCount);
		System.out.println("Total Dresses : " + dressesCount);
		
		System.out.println("Total Laptops : " + countItems(products, "laptop"));
		System.out.println("Total MobilePhones : " + countItems(products, "mobilephone"));
		System.out.println("Total Dresses : " + countItems(products,"dresses"));
		
		System.out.println("Total Products : " + totalProducts(products));
		
		int[][][] twoD = new int[3][4][5];
		System.out.println(twoD.length * twoD[0].length * twoD[0][0].length);
		
	}
	
	
	
	
	//Total Products
	static int totalProducts(Product[] products) {
		return products.length;
	}
	
	//total products whose mrp is morethan 20000
	//static int filterProductsBasedOnMrp(Product[] products)
	
	
	//List all the products who mrp is between 10000 to 20000
	
	/*static Product[] filterProductsBasedonMrp(Product[] products, double minValue, double maxValue) {
		Product[] filteredProducts = new Product[];
	}*/
	
	//List all the laptops whose mrp is between 10000 to 2000
	
	static int countItems(Product[] products,String item) {
		int count = 0;
		for(Product product : products) {
			if(product.category.equals(item)) {
				count++;
			}
		}
		return count;
	}
	
	static int countLaptops(Product[] products){
		int count = 0;
		for(Product product : products) {
			if(product.category.equals("laptop")) {
				count++;
			}
		}
		return count;
	}
	
	static int countMobilePhones(Product[] products){
		int count = 0;
		for(Product product : products) {
			if(product.category.equals("mobilephone")) {
				count++;
			}
		}
		return count;
	}
	static int countDresss(Product[] products){
		int count = 0;
		for(Product product : products) {
			if(product.category.equals("dresses")) {
				count++;
			}
		}
		return count;
	}

}
