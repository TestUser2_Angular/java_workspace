package com.java.arrays;

//What is class
public class Employee {
	// State
	// instance variables
	// What is instance variable
	// What is instance
	String name;
	String designation;
	String dept;
	char gender;
	double basicSalary;
	double hra;
	double ta;
	double da;
	int joiningYear;
	boolean isRegular;
	// Static Member
	static final String COMPANY_NAME = "Microsoft"; // Constant
	static double COMPANY_REVENUE = 23423423.23;
	static final String COMPANY_CEO = "Vinay";

	// no-arg constructor
	// What is constructor?
	Employee() {
		//System.out.println("Employee constructor is called");
		name = "abc";
		designation = "engineer";
		dept = "training";
		gender = 'm';
		basicSalary = 25000.0;
		hra = 5000;
		ta = 2500;
		da = 2000;
		joiningYear = 2019;
		isRegular = false;

	}

	public Employee(String name, String designation) {
		this.name = name;
		this.designation = designation;
	}

	public Employee(String name, String designation, String dept, char gender, double basicSalary, double hra, double ta, double da,
			int joiningYear, boolean isRegular) {

		this.name = name;
		this.designation = designation;
		this.dept = dept;
		this.gender = gender;
		this.basicSalary = basicSalary;
		this.hra = hra;
		this.ta = ta;
		this.da = da;
		this.joiningYear = joiningYear;
		this.isRegular = isRegular;
	}

	// arg constructor
	/*
	 * Employee(String n, String des, char g, double bs, double hr, double t, double
	 * d, int jy, boolean ir) { //local variables name = n; // designation = des;
	 * gender = g; basicSalary = bs; hra = hr; ta = t; da = d; joiningYear = jy;
	 * isRegular = ir;
	 * 
	 * }
	 */

	/*
	 * Employee(String name, String designation, char gender, double basicSalary,
	 * double hra, double ta, double da, int joiningYear, boolean isRegular) {
	 * 
	 * // local variables this.name = name; // this.designation = designation;
	 * this.gender = gender; this.basicSalary = basicSalary; this.hra = hra; this.ta
	 * = ta; this.da = da; this.joiningYear = joiningYear; this.isRegular =
	 * isRegular;
	 * 
	 * }
	 */
	// instance method (each instance will have its own copy)
	// What is
	// behavior(action performed by the object)
	void displayProfile() {
		System.out.println("*****************************************");
		System.out.println("Name : " + name);
		System.out.println("Designation : " + this.designation);
		System.out.println("Gender : " + this.gender);
		System.out.println("Basic Salary : " + this.basicSalary);
		System.out.println("House Rent Allownaces  : " + this.hra);
		System.out.println("Direct Allowances : " + this.da);
		System.out.println("Travelling Allowances : " + this.ta);
		System.out.println("Joining Year : " + this.joiningYear);
		System.out.println("Regular Employee Or Not : " + this.isRegular);
		System.out.println("Company Name : " + COMPANY_NAME);
		System.out.println(this.name + "'s Total Salary is : " + this.totalSalaryOfEmployee());
		System.out.println(this.name + "'s Total Experience is : " + this.calculateTotalExperienceOfEmployee());
		companyProfile();

		System.out.println("**********************************************");

	}

	// Total Salary of Employee
	// instance method
	// behavior(action)
	double totalSalaryOfEmployee() {
		// System.out.println("called");
		return this.basicSalary + this.hra + this.ta + this.da;
	}

	// Total Experience of Employee
	int calculateTotalExperienceOfEmployee() {
		int currentYear = 2021;
		return currentYear - this.joiningYear;
	}

	static void companyProfile() {

		// System.out.println("Employee Name : " + name);static can't access instance
		// data
		System.out.println("Name : " + COMPANY_NAME);
		System.out.println("Company CEO: " + COMPANY_CEO);
		System.out.println("Revenue : " + COMPANY_REVENUE);
	}

}
