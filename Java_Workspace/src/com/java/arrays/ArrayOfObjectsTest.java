package com.java.arrays;

public class ArrayOfObjectsTest {

	public static void main(String[] args) {
		Employee[] employees;
		employees = new Employee[3];
		System.out.println(employees[0]);
		// System.out.println(employees[0].name); Exception occurs
		employees[0] = new Employee("madhu", "training consultant", "training", 'm', 25000.0, 2500, 2000, 1500, 2019,
				false);
		System.out.println(employees[0].name);
		employees[1] = new Employee();
		System.out.println(employees[1].name);
		employees[2] = new Employee("vinay", "ceo", "development", 'm', 250000.0, 25000, 20000, 15000, 2010, false);
		System.out.println(employees[2].name);

		// single line declaration
		Employee[] employeesArray = {
				new Employee("madhu", "training consultant", "training", 'm', 25000.0, 2500, 2000, 1500, 2019, false),
				new Employee(),
				new Employee("vinay", "ceo", "development", 'm', 250000.0, 25000, 20000, 15000, 2010, false) };
		// for loop
		for (int index = 0; index < employeesArray.length; index += 2) {
			// System.out.println(employees[index].name);
			// employees[index].basicSalary += 1000;
			employees[index].displayProfile();
		}

		// for-each
		for (Employee employee : employeesArray) {
			// Only training department salaries should be incremented by 1000
			if (employee.dept == "development")
				employee.basicSalary += 1000;

			employee.displayProfile();
		}
	}

}
