package com.java.arrays;

public class ReverseArray {

	public static void main(String[] args) {
		int[] numbers = { 10, 20, 30, 60, 40, 50 };
		int[] reverseArray = reverseArray(numbers);
		for(int number : reverseArray) {
			System.out.println(number);
		}
	}

	// Write a method which takes an array of integers as input and gives the
	// reverse of the array

	// returntype methodname(args){}

	static int[] reverseArray(int[] inputArray) {
		int[] reverseArray = new int[inputArray.length];
		
		for (int firstIndex = inputArray.length - 1, secondIndex = 0; firstIndex >= 0; firstIndex--, secondIndex++) {
			reverseArray[secondIndex] = inputArray[firstIndex];
		}
		return reverseArray;

	}

}
