package com.java.arrays;

public class MultiDimentionalArrays {

	public static void main(String[] args) {
		//int[][] nos = new int[2][3];
		int[][] nos = {{10,20,30},{40,50,60}};
		System.out.println(nos.length);
		System.out.println(nos[0].length);
		System.out.println(nos[1].length);
		
		
		System.out.println(nos[0][0]);
		System.out.println(nos[1][2]);
		//normal for loop 
		System.out.println("-----------");
		for(int i = 0 ; i < nos.length;i++) {
			for(int j = 0 ; j < nos[i].length;j++) {
				System.out.println(nos[i][j]);
			}
		}
		System.out.println("Using for each loop");
		//for-each loop
		for(int []singleDArray : nos) {
			for(int no : singleDArray) {
				System.out.println(no);
			}
		}
		System.out.println("********");
		
		int[][][] threeD = new int[2][3][4];
		//int[][][] threeD = {{{10,20,30,40},{}}}
		//normal loop
		for(int i = 0; i < threeD.length ; i++) {
			for(int j = 0; j < threeD[i].length; j++) {
				for(int k = 0 ; k < threeD[i][j].length; k++) {
					System.out.println(threeD[i][j][k]);
				}
			}
		}
		
		//for each loop
		for( int twoD[][] : threeD) {
			for(int singleD[] : twoD) {
				for(int no : singleD) {
					System.out.println(no);
				}
			}
		}
		
		
	}

}
