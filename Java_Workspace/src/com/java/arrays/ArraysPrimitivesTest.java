package com.java.arrays;

public class ArraysPrimitivesTest {

	public static void main(String[] args) {
		//Arrays
		int[] marks;
		/* legal declarations but not according to standards
		int y[];
		int []z;
		*/
		marks = new int[5]; // instantiation
		
		System.out.println(marks.length);
		//initialize array elements
		marks[0] = 100;
		marks[1] = 200;
		marks[2] = 300;
		//marks[3] = 400;
		marks[4] = 500;
		//Array iteration
		//System.out.println(marks[6]);
		for(int index = 0; index < marks.length/2 ; index++ ) {
			System.out.println(marks[index]);
		}
		
		//for-each loop
		for(int mark : marks) {
			System.out.println(mark);
		}
		
		
		
		
		char[] gendersList;
		gendersList = new char[5];
		for(byte index = 0; index < gendersList.length; index++) {
			System.out.println(gendersList[index]);
		}
		
		boolean[] switches;
		switches = new boolean[4];
		//System.out.println(switches[0]);
		for(byte index = 0; index < switches.length; index++) {
			System.out.println(switches[index]);
		}
		
		//for-each loop
		for(boolean switchBox : switches ) {
			System.out.println(switchBox);
		}
		
		//Array single declaration
		int[] ids = {101,102,103,104};
		System.out.println(ids.length);
		for(int id : ids) {
			System.out.println(id);
		}
		

	}

}
