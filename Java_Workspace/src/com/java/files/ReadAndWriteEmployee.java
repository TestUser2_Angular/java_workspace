package com.java.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadAndWriteEmployee {

	private static final String FILE_PATH = "C://Madhu_Samala/Personal/TM/files/";

	public static void main(String[] args) throws IOException {
		String fileName = "employees.txt";
		String outputFile = "employeesdata.txt";
		
		File file = new File(FILE_PATH + fileName);
		File outFile = new File(FILE_PATH + outputFile);
		//FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(new FileReader(file));
		BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
		
		String readLine = br.readLine();
		System.out.println(readLine);
		while(readLine != null) {
		String[] empData = readLine.split(",");
		//Convert String to Primitives
		//Wrapper Classes
		//parseXXX
		//101
		
		Employee employee = new Employee(Integer.parseInt(empData[0]),
				                     empData[1],empData[2],Double.parseDouble(empData[3]));
		String empNameSalary = employee.getName() + "," +employee.getSalary() + "\n"; 
		
		bw.write(empNameSalary);
		
		//System.out.println(employee); //type@hashCode
		readLine = br.readLine();
		}
		
		br.close();
		bw.close();
		
	}
	
	//Write a method to read employees.txt file and return an array of Employees
	
	

}
