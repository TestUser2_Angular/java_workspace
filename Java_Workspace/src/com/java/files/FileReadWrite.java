package com.java.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReadWrite {
	
	private static final String FILE_PATH = "C://Madhu_Samala/Personal/TM/files/";

	public static void main(String[] args) throws IOException {		
		String fileName = "employees.txt";
		String outputFile = "employeesdata.txt";
		
		File file = new File(FILE_PATH + fileName);
		File outFile = new File(FILE_PATH + outputFile);
		//System.out.println(file.isDirectory());
		/*
		 * String[] list = file.list(); for(String name : list) {
		 * System.out.println(name); }
		 */
		File newDir = new File(FILE_PATH + "manager");
		newDir.mkdir();
		
		FileReader fr = new FileReader(file);
		FileWriter fw = new FileWriter(outFile);
		
		try {
			int readChar = fr.read();
			while(readChar!= -1) {
			//System.out.print((char)readChar);
				fw.write(readChar);
			readChar = fr.read();
			}
		} catch (IOException e) {			
			e.printStackTrace();
		} finally {
			fr.close();
			fw.close();
		}
		
		

	}

}
