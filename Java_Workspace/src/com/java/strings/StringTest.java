package com.java.strings;

public class StringTest {

	public static void main(String[] args) {
		String str = new String();
		System.out.println(str);
		//String object
		String str1 = new String("abc");
		System.out.println(str1);
		String str2 = new String("abc");
		System.out.println(str2);
		
		String str3 ;
		str3 = str1;
		System.out.println(str3);
		
		System.out.println(str1 == str2);
		
		//String literal
		String username = "mADhu";
		System.out.println(username);
		String username1 = "madhu";
		System.out.println(username == username1);
		String password = "madhu";
		
		
		char[] uname = {'m','a','d','h','u'};
		String unameStr = new String(uname);
		System.out.println(unameStr);
		
		
		
		
		//String manipulation
		
		char ch = username.charAt(4);
		System.out.println(ch);
		
		System.out.println(username.compareTo("maDhu"));
		System.out.println(username.compareToIgnoreCase("MaDhU"));
		String firstName = "madhu";
		String lastName = "samala";
		String fullName = firstName.concat(lastName);
		System.out.println(fullName);
		System.out.println(firstName);
		//String fullName = firstName + " " +lastName;
		
		System.out.println(fullName);
		//String objects are immutable?
		//System.out.println(firstName.toUpperCase());
		firstName = firstName.toUpperCase();
		System.out.println(firstName);
		
		System.out.println(fullName.contains("samwer"));
		if(fullName.contains("samala")) {
			System.out.println("He belongs to Samala's Family");
		}
		
		System.out.println(String.copyValueOf(uname));
		
		if(username.equalsIgnoreCase("madhu") && password.equals("madhu")) {
			System.out.println("Admin access");
		} else {
			System.out.println("Invalid Credentials");
		}
		String emptyStr = "  dafasd asdf      ";
		System.out.println(str.isEmpty());
		
		System.out.println(emptyStr.isEmpty());
		System.out.println(emptyStr.isBlank());
		
		System.out.println(emptyStr.length());
		
		fullName = fullName.replace('a', 'A');
		System.out.println(fullName);
		
		String email = "madhu.s.samala@gmail.com";
		
		//System.out.println(email.split("@"));
		String[] strs = email.split(".");
		System.out.println(strs.length);
		for(String s : strs) {
			System.out.println(s);
		}
		char[] emailChars = email.toCharArray();
		for(char c : emailChars) {
			System.out.print(c);
		}
		System.out.println();
		System.out.println(emptyStr.length());
		System.out.println(emptyStr.trim());
		
		byte b = 100;
		short s = 23423;
		System.out.println(String.valueOf(b));
		String.valueOf(s);
		
	}

}
