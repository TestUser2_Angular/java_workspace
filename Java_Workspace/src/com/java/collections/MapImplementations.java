package com.java.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class MapImplementations {
	
	public static void main(String[] args) {
		
		
			Map<Integer,String> map = new HashMap<>();
			map.put(101, null);
			map.put(102, null);
			map.put(103, "Madhu");
			map.put(103, "Madhu");
			map.put(103,"Madhu");
			map.put(102, "Madhu");
			map.put(102, "Madhu");
			map.put(104, null);
			map.put(null, "Madhu");
			map.put(null, "example");
			
			System.out.println(map);
			
			System.out.println(map.get(103));
			System.out.println(map.get(null));
			
			Set<Entry<Integer, String>> entrySet = map.entrySet();
			System.out.println(entrySet);
			for(Entry<Integer,String> entry : entrySet) {
				System.out.println(entry.getKey() + "->" + entry.getValue());
			}
			
			Map<Integer,Employee> employeesMap = new TreeMap<>();
			employeesMap.put(101, new Employee(101,"Madhu","Trainer",34543.43));
			employeesMap.put(104, new Employee(104,"Vinay","Developer",65765.43));
			employeesMap.put(102, new Employee(102,"Shivaji","Programmer",67683.43));
			employeesMap.put(103, new Employee(103,"prithvi","Engineer",77867.43));
			
			
			System.out.println(employeesMap);
			
			Set<Entry<Integer, Employee>> employeeMapEntrySet = employeesMap.entrySet();
			for(Entry<Integer,Employee> employeeMapEntry : employeeMapEntrySet) {
				System.out.println(employeeMapEntry.getKey());
				System.out.println(employeeMapEntry.getValue());
			}
			
			
			
			
			
			
			
	}
	
	 

}
