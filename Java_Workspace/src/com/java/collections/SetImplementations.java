package com.java.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetImplementations {
	
	public static void main(String[] args) {
		
		String[] names = {"a","c","b","d","a","c","x","l"};
		
		//HashSet
		/*
		 * No duplicates -> No order of insertion
		 */
		//LinkedHashSet
		/*
		 * No duplicates -> Order of insertion
		 */
		//TreeSet
		/*
		 * No duplicates -> sort values in ascending
		 * The object must be Comparable
		 * 
		 */
		//HashSet<String> set = new HashSet<>();
		//LinkedHashSet<String> set = new LinkedHashSet<>();
		TreeSet<String> set = new TreeSet<>();
		for(String name : names) {
			set.add(name);
		}
		//hashSet.add(new Employee());
		System.out.println(names.length);
		System.out.println(set.size());
		System.out.println(set);
		
		/*
		 * Iterator<String> iterator = set.iterator(); while(iterator.hasNext()) {
		 * String name = iterator.next(); System.out.println(name);
		 * 
		 * }
		 */
		
		//Set implementation of Employees
		Set<Employee> employees = new TreeSet<>();
		employees.add(new Employee(101,"Madhu","Trainer",34543.43));
		employees.add(new Employee(104,"Vinay","Developer",65765.43));
		employees.add(new Employee(103,"prithvi","Engineer",77867.43));
		employees.add(new Employee(102,"Shivaji","Programmer",67683.43));
		employees.add(new Employee(102,"Shivaji","Programmer",67683.43));
		System.out.println(employees);
		
		for(Employee employee : employees) {
			System.out.println(employee);
		}
		
		
		
	}

}
