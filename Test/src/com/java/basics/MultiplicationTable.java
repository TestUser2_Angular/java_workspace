package com.java.basics;

public class MultiplicationTable {
	
	public static void main(String[] args) {
		for(int no=1 ; no <= 100; no++) {
		for(int i = 1; i <= 10; i++) {
			System.out.println(no +" * "+ i + " = " +(no*i));
		}
		System.out.println("----------------------");
		}
	}

}
